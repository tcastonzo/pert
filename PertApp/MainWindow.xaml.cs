﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PertApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        
        public MainWindow()
        {
            InitializeComponent();
            
            
            this.WildlyOptimistic.Text = @"0";
            this.GreatestChanceOfSuccess.Text = @"0";
            this.WildlyPessimistic.Text = @"0";
            this.Estimate.IsReadOnly = true;
            this.StandardDeviation.IsReadOnly = true;

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            var textBox = sender as TextBox;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            decimal o;
            decimal n;
            decimal p;

            if ((!Decimal.TryParse(this.WildlyOptimistic.Text, out o) || string.IsNullOrEmpty(this.WildlyOptimistic.Text)) ||
                (!Decimal.TryParse(this.GreatestChanceOfSuccess.Text, out n) || string.IsNullOrEmpty(this.GreatestChanceOfSuccess.Text)) ||
                (!Decimal.TryParse(this.WildlyPessimistic.Text, out p) || string.IsNullOrEmpty(this.WildlyPessimistic.Text))
                
                )
            {
                MessageBox.Show(@"Please enter only numbers", "Alert");
            } else {
                
                Calculate();
            }
            
        }

        private void Calculate()
        {
            decimal o = Convert.ToDecimal(WildlyOptimistic.Text);
            decimal n = Convert.ToDecimal(GreatestChanceOfSuccess.Text);
            decimal p = Convert.ToDecimal(WildlyPessimistic.Text);

            decimal est = Math.Round((o + (4*n) + p)/6,2,MidpointRounding.ToEven);
            decimal std = Math.Round((p - o)/6,2,MidpointRounding.ToEven);

            Estimate.Text = est.ToString();
            StandardDeviation.Text = std.ToString();



        }
    }
}
